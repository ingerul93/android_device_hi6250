#
# Copyright 2019 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# This contains the module build definitions for the hardware-specific
# components for this device.
#
# As much as possible, those components should be built unconditionally,
# with device-specific names to avoid collisions, to avoid device-specific
# bitrot and build breakages. Building a component unconditionally does
# *not* include it on all devices, so it is safe even with hardware-specific
# components.

DEVICE_PATH := device/huawei/hi6250

# 64 Bit
TARGET_IS_64_BIT := true
TARGET_USES_64_BIT_BINDER := true

# Architecture
TARGET_ARCH := arm64
TARGET_ARCH_VARIANT := armv8-a
TARGET_CPU_ABI := arm64-v8a
TARGET_CPU_ABI2 :=
TARGET_CPU_VARIANT := cortex-a53

TARGET_2ND_ARCH := arm
TARGET_2ND_ARCH_VARIANT := armv8-a
TARGET_2ND_CPU_ABI := armeabi-v7a
TARGET_2ND_CPU_ABI2 := armeabi
TARGET_2ND_CPU_VARIANT := cortex-a53

# Assert
TARGET_OTA_ASSERT_DEVICE := hi6250

# Board
BOARD_HAS_NO_SELECT_BUTTON := true
BOARD_BUILD_SYSTEM_ROOT_IMAGE := true
TARGET_BOARD_PLATFORM := hi6250

# File System
BOARD_BUILD_SYSTEM_ROOT_IMAGE := true
TARGET_USERIMAGES_USE_EXT4 := true
TARGET_USERIMAGES_USE_F2FS := true
TARGET_USES_MKE2FS := true

# Kernel
BOARD_KERNEL_BASE := 0x10000000
BOARD_KERNEL_PAGESIZE := 2048
BOARD_MKBOOTIMG_ARGS := --kernel_offset 0x8000 --ramdisk_offset 0x01000000 --tags_offset 0x0100

BOARD_KERNEL_CMDLINE := androidboot.selinux=permissive

BOARD_KERNEL_IMAGE_NAME := Image
TARGET_NO_KERNEL := false
TARGET_PREBUILT_KERNEL := /dev/null

TARGET_PREBUILT_KERNEL := device/huawei/hi6250/dummykernel

# NTFS (R/W access)
TW_INCLUDE_NTFS_3G := true

# Partitions
BOARD_BOOTIMAGE_PARTITION_SIZE := 25165824
BOARD_FLASH_BLOCK_SIZE := 131072 # (BOARD_KERNEL_PAGESIZE * 64)
BOARD_RECOVERYIMAGE_PARTITION_SIZE := 33554432
BOARD_SYSTEMIMAGE_PARTITION_SIZE := 5905580032
BOARD_USERDATAIMAGE_PARTITION_SIZE := 119663493120

TARGET_COPY_OUT_VENDOR := vendor

# Properties
TARGET_SYSTEM_PROP := device/huawei/hi6250/system.prop

# Ramdisk
BOARD_CUSTOM_BOOTIMG_MK := device/huawei/hi6250/custombootimg.mk

# Recovery
BOARD_SUPPRESS_SECURE_ERASE := true
BOARD_VOLD_EMMC_SHARES_DEV_MAJOR := true
RECOVERY_SDCARD_ON_DATA := true

TW_BRIGHTNESS_PATH := /sys/class/leds/lcd_backlight0/brightness
TW_CUSTOM_BATTERY_PATH := /sys/class/power_supply/Battery
TW_DEFAULT_BRIGHTNESS := "2048"
TW_EXCLUDE_DEFAULT_USB_INIT := true
TW_NO_HAPTICS := true
TW_NO_SCREEN_BLANK := true
TW_THEME := portrait_hdpi
TW_INCLUDE_EXFAT_FUSE := true
#TW_USE_TOOLBOX := true

# SPECIAL FLAGS
# ignore a factory reset when using the phone's factory reset screen
# or since N (at least) when using <fastboot erase userdata> which set the same
# https://gerrit.omnirom.org/#/c/20750/
# this flag requires to build TWRP in branch 7.1 or later
# DO NOT SET TO FALSE WHEN YOU DO NOT KNOW WHAT YOU'RE DOING!
TW_IGNORE_MISC_WIPE_DATA := true

# SELinux
# Ignore neverallow errors
SELINUX_IGNORE_NEVERALLOWS := true
#BOARD_PLAT_PRIVATE_SEPOLICY_DIR += $(DEVICE_PATH)/sepolicy/private
#BOARD_PLAT_PUBLIC_SEPOLICY_DIR += $(DEVICE_PATH)/sepolicy/public
BOARD_SEPOLICY_DIRS += device/huawei/hi6250/sepolicy
include vendor/omni/sepolicy/sepolicy.mk

# CRYPTO
#TW_INCLUDE_CRYPTO := true
#TW_INCLUDE_CRYPTO_FBE := true

# Extras
TW_EXCLUDE_TWRPAPP := true

# Debug
TWRP_INCLUDE_LOGCAT := true
TARGET_USES_LOGD := true

